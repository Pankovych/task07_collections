package com.epam.passive.model;

public class Goods {
    private String type;
    private String producer;
    private String description;
    private double price;

    public Goods(final String type, final String producer, final double price, final String description) {
        this.type = type;
        this.producer = producer;
        this.price = price;
        this.description = description;
    }

    public final String getDescription() {
        return description;
    }

    public final String getType() {
        return type;
    }

    public final String getProducer() {
        return producer;
    }

    public final double getPrice() {
        return price;
    }
}
