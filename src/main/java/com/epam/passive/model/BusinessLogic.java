package com.epam.passive.model;

import java.util.*;
import org.apache.logging.log4j.*;

public class BusinessLogic implements Model {

    private static Logger logger1 = LogManager.getLogger(BusinessLogic.class);
    private int[] arr1;
    private int[] arr2;
    private static Random rand = new Random();
    private static Scanner input = new Scanner(System.in);

    public BusinessLogic(int size) {
        arr1=new int[size];
        arr2=new int[size];

        for(int i=0;i<size;i++){
            arr1[i]=rand.nextInt(100);
            arr2[i]=rand.nextInt(100);
        }
    }

    public final void unionArray() {
        int iterArr3=0;
        int[] arr3;
        for(int i=0;i<arr1.length;i++){
            for(int j=0;j<arr2.length;j++){
                if(arr1[i]==arr2[j]){
                    iterArr3++;
                    break;
                }
            }
        }
        arr3=new int[iterArr3];
        iterArr3=0;
        for(int i=0;i<arr1.length;i++){
            for(int j=0;j<arr2.length;j++){
                if(arr1[i]==arr2[j]){
                    arr3[iterArr3]=arr1[i];
                    iterArr3++;
                    break;
                }
            }
        }
        for(int i=0;i<arr3.length;i++){
            System.out.print(" "+arr3[i]);
        }
    }

    public final void containsOneMasive(){
        int iterArr3=0;
        int check=0;
        int[] arr3;
        for(int i=0;i<arr1.length;i++){
            for(int j=0;j<arr2.length;j++){
                if(arr1[i]==arr2[j]){
                    check++;
                    break;
                }
            }
            if(check==0){
                iterArr3++;
            }else{
                check=0;
            }
        }
        arr3=new int[iterArr3];
        iterArr3=0;
        for(int i=0;i<arr1.length;i++){
            for(int j=0;j<arr2.length;j++){
                if(arr1[i]==arr2[j]){
                    check++;
                    break;
                }
            }
            if(check==0){
                arr3[iterArr3]=arr1[i];
            }else{
                check=0;
            }
        }
        for(int i=0;i<arr3.length;i++){
            System.out.print(" "+arr3[i]);
        }
    }



    public final void doorGame(){
        int[] door = new int[10];
        int hero=25;
        int deathDoor=0;
        int choose;
        List<Integer> countClosedDoors=new ArrayList<>();
        int countDoorWithArtifacts=rand.nextInt(10);
        for(int i=0;i<10;i++){
            door[i]=-1*rand.nextInt(100);
            countClosedDoors.add(i+1);
        }
        for(int i=0;i<countDoorWithArtifacts;i++){
            door[rand.nextInt(10)]=rand.nextInt(80);
        }

        for(int i=0;i<10;i++){
            if(door[i]*-1>25){
                deathDoor++;
            }
        }

        logger1.info("Death dorr is "+deathDoor);

        for(int i=0;i<10;i++){
            logger1.info("Choose the door");
            for(int s:countClosedDoors){
                System.out.print(s+" ");
            }
            System.out.println("");
            choose=input.nextInt();
            if(countClosedDoors.contains(choose)) {
                if (door[choose - 1] > 0) {
                    logger1.info("Its Artifact with power" + door[choose - 1]);
                    hero += door[choose - 1];
                    countClosedDoors.remove(countClosedDoors.indexOf(choose));
                }else{
                    logger1.info("Its Monster with power" + door[choose - 1]);
                    hero += door[choose - 1];
                    countClosedDoors.remove(countClosedDoors.indexOf(choose));
                }
                logger1.info("Hero power-"+hero);
                if(hero<=0){
                    logger1.info("Hero die");
                    break;
                }

            }else{
                logger1.info("this door is already open");
            }
        }
        if(hero>0){
            logger1.info("Hero win");
        }
    }
}
