package com.epam.passive.controller;

import com.epam.passive.model.*;

import java.util.List;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic(100);
    }

    @Override
    public final void unionArray() {
        model.unionArray();
    }

    @Override
    public final void containsOneMasive(){
        model.containsOneMasive();
    }

    @Override
    public final void doorGame(){
        model.doorGame();
    }
}
